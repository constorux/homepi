const express = require('express');
const GERTEN_PREFIX = "/garten";
const fs = require("fs");
const DATA_FILE_PATH = 'garten/data/plants.json';


class GartenManager {
    constructor(expressInstance, port) {
        console.log("GARTEN | starting..");

        expressInstance.use(GERTEN_PREFIX, express.static('public/garten'));

        // return all plants to the client
        expressInstance.get('/api' + GERTEN_PREFIX, (req, res) => {
            this.readDataFile((data) => { res.send(data) });
        });

        // delete a plant
        expressInstance.post('/api' + GERTEN_PREFIX + '/delete', (req, res) => {
            if (req.body.id == null) {
                res.send({ success: false });
                return;
            }

            this.deletePlant(req.body.id, () => { res.send({ success: true }) });
        });

        // return all plants to the client
        expressInstance.post('/api' + GERTEN_PREFIX + '/edit', (req, res) => {
            var plant = req.body.plant;
           
            if (plant == null) {
                res.send(JSON.stringify({ success: false, id: null }));
                return;
            }

            this.editPlant(plant, (id) => { res.send(JSON.stringify({ success: true, id: id })) });
        });

        console.log("GARTEN | is running");
    }

    deletePlant(id, onFinish) {
        this.readDataFile((data) => {
            data.plants = data.plants.filter(plant => plant.id != id);
            this.writeDataFile(data, onFinish);
        })
    }

    editPlant(editedPlant, onFinish) {
        this.readDataFile((data) => {
            if (editedPlant.id == null) {
                let newId = 0;
                for (var plant of data.plants) {
                    if (newId <= plant.id) {
                        newId++;
                    }
                }
                editedPlant.id = newId;
                data.plants.push(editedPlant);
            }
            else {
                for (var i = 0; i < data.plants.length; i++) {
                    if (data.plants[i].id == editedPlant.id) {
                        data.plants[i] = editedPlant;
                    }
                }
            }

            this.writeDataFile(data, () => { onFinish(editedPlant.id) });
        })
    }

    readDataFile(onData) {
        fs.readFile(DATA_FILE_PATH, function (err, data) {
            if (err) {
                return console.error(err);
            }
            onData(JSON.parse(data));
        });
    }

    writeDataFile(contents, onSave) {
        fs.writeFile(DATA_FILE_PATH, JSON.stringify(contents), function (err) {
            if (err) {
                return console.error(err);
            }
            onSave();
        });
    }
}

module.exports = GartenManager;
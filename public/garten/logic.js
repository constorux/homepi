// grundstück Breite: 23m

const METER_PIXEL_FACTOR = 36.956;
const MARGIN_OFFSET = 50;

const PLANT_TYPES = [{
    name: "Busch",
    id: "bush",
    color: "#00a896"
},
{
    name: "Baum",
    id: "tree",
    color: "#DCE364"
},
{
    name: "Hecke",
    id: "hedge",
    color: "#028090"
},
{
    name: "Strauch",
    id: "shrub",
    color: "#02c39a"
}
]

const API = {
    hostname: 'fairpi.local',
    path: '/api/garten',
    port: 81
}

function getData() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", API.path, false);
    xmlHttp.send(null);
    return JSON.parse(xmlHttp.responseText);
}

function deletePlant(id) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", API.path + "/delete", false);
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    xmlHttp.send(JSON.stringify({ id: id }));
    if (JSON.parse(xmlHttp.responseText).success == true) {
        drawData(getData());
        hideEditOverlay();
    }
}

function editPlant(plant) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", API.path + "/edit", false);
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    xmlHttp.send(JSON.stringify({ plant: plant }));
    var response = JSON.parse(xmlHttp.responseText);
    if (response.success == true) {
        drawData(getData());
        plant.id = response.id;
        showEditOverlay(plant);
    }
}


const enableOverlay = document.getElementById("enableOverlay");
const enableAllButton = document.getElementById("enableAllButton");

function initHandlers() {

    document.getElementById("editOverlay").addEventListener('click', (event) => {
        event.stopPropagation();
    });

    document.getElementById("body").addEventListener('click', (event) => {
        hideEditOverlay();
    });

    enableAllButton.addEventListener('click', (event) => {
        for (let childNode of dataSVG.children) {
            childNode.classList.remove("plantDisabled");
        }
        enableOverlay.style.visibility = "hidden";
    });

    function svgOnClick(event) {
        console.log("click on canvas");
        event.stopPropagation();
        var newPlant = {
            position: {
                x: ((event.layerX - MARGIN_OFFSET) / METER_PIXEL_FACTOR).toFixed(2),
                y: ((event.layerY - MARGIN_OFFSET) / METER_PIXEL_FACTOR).toFixed(2),
            },
            properties: {
                width: null,
                depth: null,
                rotation: null
            },
            id: null,
            name: "",
            description: ""
        }
        console.log(newPlant);
        showEditOverlay(newPlant);
    }

    var dataSVG = document.getElementById("dataSVG");
    dataSVG.addEventListener('click', svgOnClick);
}

function getColorFromType(type){
    if(type == null || type === ""){
        return "#008080";
    }
    return PLANT_TYPES.find((element) => element.id === type).color;
}

function drawData(data) {
    while (dataSVG.firstChild) {
        dataSVG.removeChild(dataSVG.firstChild);
    }

    for (let i = 0; i < data.plants.length; i++) {
        addSVGPlant(data.plants[i], i);
    }
}


function removeSVGPlant(id) {
    let element = document.getElementById(id);
    if (element != null) {
        dataSVG.removeChild(element);
    }
}

function addSVGPlant(plant) {
    let color;
    if(plant.id != null){
       color = getColorFromType(plant.type) ?? "#008080";
    }
    else{
        color = "#a37774";
    }

    let newElement = document.createElementNS("http://www.w3.org/2000/svg", 'ellipse');
    newElement.setAttribute("cx", plant.position.x * METER_PIXEL_FACTOR + MARGIN_OFFSET);
    newElement.setAttribute("cy", plant.position.y * METER_PIXEL_FACTOR + MARGIN_OFFSET);
    newElement.setAttribute("rx", (plant.properties.width != null) ? plant.properties.width * METER_PIXEL_FACTOR : 1 * METER_PIXEL_FACTOR);
    newElement.setAttribute("ry", (plant.properties.depth != null) ? plant.properties.depth * METER_PIXEL_FACTOR : 1 * METER_PIXEL_FACTOR);
    newElement.setAttribute("transform", "rotate(" + (plant.properties.rotation ?? 0) + "," + (plant.position.x * METER_PIXEL_FACTOR + MARGIN_OFFSET) + "," + (plant.position.y * METER_PIXEL_FACTOR + MARGIN_OFFSET) + ")");
    newElement.style.fill = color;
    newElement.style.fillOpacity = 0.5;
    newElement.style.stroke = color;
    newElement.style.strokeWidth = "2px";
    newElement.id = plant.id;

    newElement.addEventListener('click', (event) => {
        if (!newElement.classList.contains("plantDisabled")) {
            showEditOverlay(plant);
            event.stopPropagation();
        }

    });

    newElement.addEventListener('contextmenu', (ev) => {
        ev.preventDefault();
        newElement.classList.add("plantDisabled");
        enableOverlay.style.visibility = "visible";
    });

    dataSVG.appendChild(newElement);
}

function hightlightOnePlant(id) {
    for (let element of dataSVG.children) {
        if (element.id == id) {
            element.style.strokeWidth = "6px";
            continue;
        }
        element.style.strokeWidth = "2px";
    }
}


const editOverlay = document.getElementById("editOverlay");
const progressIndicator = document.getElementById("editLoadingIndicator");
var deleteButton = document.getElementById("editDeleteButton");
var saveButton = document.getElementById("editSaveButton");

const editPName = document.getElementById("pName");
const editPType = document.getElementById("pType");
const editPWidth = document.getElementById("pWidth");
const editPDepth = document.getElementById("pDepth");
const editPRotation = document.getElementById("pRotation");
const editPXPos = document.getElementById("pXPos");
const editPYPos = document.getElementById("pYPos");
const editPDescription = document.getElementById("pDescription");

function showEditOverlay(plant) {

    progressIndicator.style.display = "none";
    saveButton.style.display = "block";

    if (plant.id == null) {
        removeSVGPlant("null");
        addSVGPlant(plant, -1);
        hightlightOnePlant("null");

    } else {
        removeSVGPlant("null");
        hightlightOnePlant(plant.id);
    }

    editOverlay.style.visibility = "visible";

    editPName.value = plant.name;
    editPType.value = plant.type ?? "bush";
    editPDepth.value = plant.properties.depth;
    editPWidth.value = plant.properties.width;
    editPRotation.value = plant.properties.rotation;
    editPXPos.value = plant.position.x;
    editPYPos.value = plant.position.y;
    editPDescription.value = plant.description;

    // delete old listeners
    deleteButton.parentNode.replaceChild(deleteButton.cloneNode(true), deleteButton);
    saveButton.parentNode.replaceChild(saveButton.cloneNode(true), saveButton);
    deleteButton = document.getElementById("editDeleteButton");
    saveButton = document.getElementById("editSaveButton");

    deleteButton.style.visibility = (plant.id != null) ? "visible" : "hidden";
    deleteButton.addEventListener('click', (event) => {
        progressIndicator.style.display = "block";
        saveButton.style.display = "none";
        console.log("delete " + plant.id);
        deletePlant(plant.id);
    });

    saveButton.addEventListener('click', (event) => {

        progressIndicator.style.display = "block";
        saveButton.style.display = "none";
        let newPlantValues = {
            position: {
                x: Number.parseFloat(editPXPos.value),
                y: Number.parseFloat(editPYPos.value)
            },
            properties: {
                width: (editPWidth.value != "") ? Number.parseFloat(editPWidth.value) : 1,
                depth: (editPDepth.value != "") ? Number.parseFloat(editPDepth.value) : 1,
                rotation: (editPRotation.value != "") ? Number.parseFloat(editPRotation.value) : 0
            },
            name: (editPName.value != "") ? editPName.value : "Namenlose Pflanze",
            type: (editPType.value != "") ? editPType.value : "bush",
            description: (editPDescription.value != "") ? editPDescription.value : "",
            id: plant.id,
        }

        editPlant(newPlantValues);
    });
}

function addTreeTypeOptions() {
    for (let type of PLANT_TYPES) {
        let newElement = document.createElement("option");
        newElement.setAttribute("value", type.id);
        newElement.innerHTML = type.name;
        editPType.appendChild(newElement);
    }
}

function hideEditOverlay() {
    deleteButton.style.visibility = "hidden";
    editOverlay.style.visibility = "hidden";
    removeSVGPlant("null");
    hightlightOnePlant(-1);
}






initHandlers();
addTreeTypeOptions();
drawData(getData());
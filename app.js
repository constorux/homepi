var TempPi = require("./tempPi/server");
var GartenManager = require("./garten/server");
const express = require('express');
const bodyParser = require("body-parser");

const port = 80;
const app = new express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

function startServer(){
    app.listen(port, () => console.log("HOMEPI | active at port: " + port));
}


var tempPi = new TempPi(app,port);
var gartenMan = new GartenManager(app,port);
startServer();
const TelegramBot = require('./telegramBot');
const TempAPI = require('./tempAPI');



class TempServer {
    constructor(expressInstance, port) {
        console.log("TEMPPI | starting..");

        var tempAPI = new TempAPI(expressInstance);

        // delaying because of DNS resolve error
        setTimeout(function () {
            var telegramBot = new TelegramBot(port);
            console.log("TEMPPI | TelegramBot (after delay) is running");
        }, 5000);
        console.log("TEMPPI | is running");
    }
}

module.exports = TempServer;


const { Telegraf } = require('telegraf');
const http = require('http');
const fs = require("fs");

const API = {
    hostname: 'localhost',
    path: '/api/temp',
    method: 'GET',
    port: 80
}

class TelegramBot {

    errorFlag = false;
    warningFlag = false;
    bot;

    constructor(port) {

        this.bot = new Telegraf("1354392786:AAHEhaVJ4aXX1pvhGr4bExsKmDian0NjnJ8");

        API.port = port;
        console.log("TEMPPI | starting telegram bot");

        this.bot.start((ctx) => {
            this.saveChatID(ctx.chat.id)
            ctx.reply('Willkommen!\nDu bekommst nun Updates zur Heizungs-Temperatur von mir');
        });
        //bot.help((ctx) => ctx.reply('Send me a sticker'))
        //bot.on('sticker', (ctx) => ctx.reply('👍'))
        this.bot.command('temperatur', ({ reply }) => this.getTempString((text) => reply(text)))
        this.bot.hears('Hallo', (ctx) => { ctx.reply('Ich bin aktiv!'); })
        this.bot.hears('outputlog', (ctx) => { ctx.reply("HomePI LOG:\r\n\r\n" + this.readLogFile("output")); })
        this.bot.hears('errorlog', (ctx) => { ctx.reply("HomePI ERROR-LOG:\r\n\r\n" + this.readLogFile("errors")); })
        this.bot.launch()

        //send startup message:
        this.sendDebugBroadcast("Der Server wurde neu gestartet.");

        this.startInterval();

        var errorLine = 0;
        fs.watchFile('logs/errors.log', (curr, prev) => {
            var data = this.readLogFile("errors").split("\n");

            var message = "";
            for (errorLine; errorLine < data.length; errorLine++) {
                message = message + data[errorLine] + "\n";
            }
            if (message !== "") {
                this.sendDebugBroadcast("SERVER ERROR:\n\n" + message);
            }
        });

        //this.sendDebugBroadcast(fs.readFileSync(1).toString());

        // print system output to debug
        //var stdin = fs.readFileSync(1).toString();

        /*stdin.addListener("data", function (d) {
            // note:  d is an object, and when converted to a string it will
            // end with a linefeed.  so we (rather crudely) account for that  
            // with toString() and then trim() 
            this.sendDebugBroadcast("LOG: " + d.toString().trim);
        });*/
    }





    // - helper functions

    readLogFile(type) {
        return fs.readFileSync('logs/' + type + '.log', 'UTF-8');
    }

    getAllChatIDs(file) {
        const data = fs.readFileSync('tempPi/' + file + '.txt', 'UTF-8');
        return data.split(/\r?\n/);
    }

    saveChatID(chatID) {
        var users = this.getAllChatIDs("users");
        if (users.length === 0 || users.findIndex((element) => element == chatID) < 0) {
            fs.appendFileSync('tempPi/users.txt', chatID + "\r\n");
        }
    }

    sendMessageToAll(message) {
        for (var chatID of this.getAllChatIDs("users")) {
            if (chatID !== "" && chatID != null) {
                this.bot.telegram.sendMessage(chatID, message);
            }
        }
    }

    sendDebugBroadcast(message) {
        for (var chatID of this.getAllChatIDs("debug")) {
            if (chatID !== "" && chatID != null) {
                this.bot.telegram.sendMessage(chatID, "DEBUG: " + message);
            }
        }
    }

    sendAPIrequest(callback) {
        http.get(API, (resp) => {
            let data = '';
            resp.on('data', (chunk) => {
                data += chunk;
            });
            resp.on('end', () => {
                this.errorFlag = false;
                callback(JSON.parse(data));
            });

        }).on("error", (err) => {
            if (!this.errorFlag) {
                this.errorFlag = true;
                this.sendMessageToAll("Ich kann nicht mit den Sensoren kommunizieren.\r\nBitte wende dich an den Entwickler.");
            }
        });
    }

    startInterval() {
        return setInterval(() => {
            this.sendAPIrequest((values) => {
                if (!values || !values.sensors || !values.sensors[0] || !values.sensors[1] || !values.sensors[0].value || !values.sensors[1].value) {
                    if (!this.errorFlag) {
                        this.errorFlag = true;
                        this.sendMessageToAll("Die Daten der Sensoren sind nicht valide.\r\nBitte wende dich an den Entwickler.");
                    }
                    return;
                }
                this.errorFlag = false;
                var boilerTemp = values.sensors[0].value;
                var vorlaufTemp = values.sensors[1].value;

                if (boilerTemp <= 45 && vorlaufTemp <= 45) {
                    if (!warningFlag) {
                        warningFlag = true;
                        this.sendMessageToAll("Warnung!\r\n\r\nBoiler: " + boilerTemp + "°C\r\nVorlauf: " + vorlaufTemp + "°C")
                    }
                    return;
                }
                this.warningFlag = false;
            });
        }, 60000);
    }


    getTempString(callback) {
        this.sendAPIrequest((values) => {
            if (!values || !values.sensors) {
                callback("Die Daten der Sensoren sind nicht valide.\r\nBitte wende dich an den Entwickler.");
                return;
            }
            var result = "Momentane Temperaturen:\r\n\r\n"
            for (var sensor of values.sensors) {
                result = result + sensor.name + ": " + sensor.value + "°C" + "\r\n";
            }
            callback(result);
        })
    }
}

module.exports = TelegramBot;
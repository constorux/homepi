const { exec } = require("child_process");
const http = require('http');
const express = require('express');

class TempAPI {

    constructor(expressInstance) {
        console.log("TEMPPI | starting API");
        //serve static pages
        expressInstance.use("/", express.static('public/tempPi'));
        
        expressInstance.get('/api', (req, res) => {
            res.send("API of the HomePI");
        });


        expressInstance.get('/api/wallpaper', (req, res) => {
            this.getWallpaper(res);
        });


        expressInstance.get('/api/temp', (req, res) => {
            var result = [
                { id: "boiler", name: "Wasserboiler", state: "OKAY", value: 0 },
                { id: "flow", name: "Vorlauf", state: "OKAY", value: 0 },
                { id: "radiator", name: "Heizung", state: "OKAY", value: 0 },
                { id: "room", name: "Raumtemeratur", state: "WARNING", value: 0 },
                { id: "server", name: "Server", state: "GOOD", value: 0 },
            ];

            Promise.all([this.execPromise(1), this.execPromise(2), this.execPromise(3), this.execPromise(4), this.execPromise(5)]).then((values) => {

                for (var i = 0; i < values.length; i++) {
                    result[i].value = (Number.parseInt(values[i])) || undefined;
                }

                result[0].state = this.setState(result[0].value, 0, -1, 56, 80, 54, 80, 0, 80);
                result[1].state = this.setState(result[1].value, result[0].value + 1, 80);
                result[2].state = this.setState(result[2].value, 0, -1, 65, 80, 45, 80, 0, 80);
                result[3].state = this.setState(result[3].value, 0, -1, 0, 20, 0, 37, 0, 45);
                result[4].state = this.setState(result[4].value, 0, -1, 0, 25, 0, 45, 0, 60);

                res.send({ sensors: result });
            });

        });

    }

    execPromise(sensorID) {
        return new Promise(async resolve => {
            exec("tempsens " + sensorID, (err, stout, sterr) => {
                resolve(stout)
            });
        });
    }


    setState(value, activeLB = 0, activeUB = -1, goodLB = 0, goodUB = 80, okayLB = 0, okayUB = 80, warnLB = 0, warnUB = 80) {
        if (value <= activeUB && value >= activeLB) {
            return "ACTIVE";
        }
        if (value <= goodUB && value >= goodLB) {
            return "GOOD";
        }
        if (value <= okayUB && value >= okayLB) {
            return "OKAY";
        }
        if (value <= warnUB && value >= warnLB) {
            return "WARNING";
        }
        if (value > warnUB || value < warnLB) {
            return "DANGER";
        }
        return "NONE"
    }

    getWallpaper(res) {


        var wallpaperDEFAULT = { url: "wallpaper.jpg", info: "Wald nahe der Kastelburg, Waldkirch", link: undefined };
        var wallpaper = { url: undefined, info: undefined, link: undefined };

        var options = {
            host: 'www.bing.com',
            path: '/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=de-DE'
        };
        var request = http.request(options, (result) => {
            this.getResponse(result).then((toSend) => {

                try {
                    var resultJSON = JSON.parse(toSend);
                    if (resultJSON !== undefined && resultJSON.images !== undefined && resultJSON.images[0] !== undefined
                        && resultJSON.images[0].url !== undefined && resultJSON.images[0].copyright !== undefined) {
                        wallpaper.info = resultJSON.images[0].copyright + " (via Bing)";
                        wallpaper.url = "https://www.bing.com" + resultJSON.images[0].url;
                        wallpaper.link = "https://www.bing.com";
                        res.send(wallpaper);
                        return;

                    }
                } catch (e) {
                    //on error send default image
                }
                res.send(wallpaperDEFAULT);
            });
        }).end();

        request.on('error', (err) => {
            res.send(wallpaperDEFAULT);
        });
    }

    getResponse(response) {
        return new Promise((resolutionFunc, rejectionFunc) => {
            var str = ''
            response.on('data', function (chunk) {
                str += chunk;
            });

            response.on('end', function () {
                resolutionFunc(str);
            });
        });
    }
}

module.exports = TempAPI;
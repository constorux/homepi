# HomePi

A raspberry pi that functions as a server for multiple applications around the home

### Installation

In order to run the project the following things must be true:
- the custom tempsens command has to be installed
    - copy the `tempsens` file to `/bin`
- The System has *node.js* installed (for development *npm* is also helpful). This can be done through running 
    - `curl -sL https://deb.nodesource.com/setup_current.x | sudo bash -`
    - `sudo apt install nodejs`
- The project is placed at: `/home/pi/homePi`
- Add the Project to the autostart:
    - edit the *crontab* environment by running `crontab -e`
    - adding the line `@reboot cd /home/pi/homePi && sudo node app.js > /home/pi/homePi/logs/output.log 2> /home/pi/homePi/logs/errors.log`

##### multiple hosts:
- Add the subdomains to `sudo nano /etc/hosts` as: `127.0.0.1   sub.fairpi.local`
